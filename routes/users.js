var express = require('express');
var router = express.Router();
var passport = require ('passport');
var LocalStrategy = require ('passport-local').Strategy;

var User = require('../models/user');

// Register
router.get('/register', function (req, res) {
	res.render('register');
});

// Login
router.get('/login', function (req, res) {
	res.render('login');
});

// Home
router.get('/home', function (req, res) {
	res.render('home');
})

// Register User
router.post('/register', function (req, res) {
	var name = req.body.name;
	var username = req.body.username;
	var email = req.body.email;
	var password = req.body.password;
	var cpassword = req.body.cpassword;
	var youtubech = req.body.youtubech;
	var fbpa = req.body.fbpa;
	var twitter = req.body.twitter;

	// Validaton
	req.checkBody('name', 'Se requiere un nombre').notEmpty();
	req.checkBody('email', 'Se requiere un correo').notEmpty();
	req.checkBody('email', 'Este email no es valido').isEmail();
	req.checkBody('username', 'Se requiere un Nombre de Usuario').notEmpty();
	req.checkBody('password', 'Se requiere Contraseña').notEmpty();
	req.checkBody('cpassword', 'No coinciden').equals(req.body.password);

	var errors = req.validationErrors();

	if (errors) {
		res.render('register',{
			errors:errors
		})
	} else {
		var newUser = new User({
			name: name,
			username: username,
			email: email,
			password: password,
			youtubech: youtubech,
			fbpa: fbpa,
			twitter: twitter
		});

		User.createUser(newUser, function (err, user) {
			if (err) throw err;
			console.log(user);
		});

		req.flash('succes_msg', 'Has sido registrado, ahora inicia sesion');

		res.redirect('login');
	}

});

// Passport
passport.use(new LocalStrategy(
  function (username, password, done) {
  	User.getUserByUsername(username, function (err, user) {
  		if (err) throw err;
  		if (!user) {
  			return done(null, false, {massage: 'NO REGISTRADO'});
  		}

  		User.comparePassword(password, user.password, function (err, isMatch) {
  			if (err) throw err;
  			if (isMatch) {
  				return	done(null, user);	
  			} else {
  				return	done(null, false, {message: 'PASSWORD ERROR'})
  			}
  		});
  	});
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});


router.post('/login',
  passport.authenticate('local', {succesRedirect: '/', failureRedirect:'/users/login', failureFlash:true}),
  function(req, res) {
    res.redirect('home');
  });

router.get('/logout', function (req, res) {
	req.logout();

	req.flash('succes_msg', 'Gacias por visitar');

	res.redirect('/users/login');
});

router.get('/user', function(req, res) {
    res.render('home', {username: req.user.username });
});

module.exports = router;