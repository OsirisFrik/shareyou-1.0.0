var express = require('express');
var router = express.Router();

// Get Homepage
router.get('/', ensureAuthenticated, function (req, res) {
	x.getUserById("5722f61f3f482f642c26d953", function(err,res){
		console.log(err);
		console.log(res);
	});
	res.render('index');
});

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	} else {
		req.flash('error_msg', 'No estas logeado');
		res.redirect('/users/login');
	}
}

module.exports = router;